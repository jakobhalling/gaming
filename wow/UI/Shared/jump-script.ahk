#SingleInstance, Force
CoordMode, TOolTip

ToolTip, Script on!, % A_SCreenWidth // 2, 0 1 

startTime := A_TickCount
SetTimer, StartJumping, 10
return

StartJumping:
if ((A_TickCount - startTime) < 24000000)
  {
    Send {Space}
    random, sleepFor, 180000, 240000
    Sleep, % sleepFor
  }
else
  {    ; if it is past 40 minutes, turn off the timer.
    SetTimer, StartJumping, Off
  }
return

^Esc::ExitApp