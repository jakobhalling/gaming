## NVIDIA CONTROL PANEL 3D Settings

* **Image Sharpening** can be set according to your preference. Try setting it on 50 and change it as per requirement. 
* **Anisotropic filtering** OFF
* **Ambient Occlusion** OFF
* **Antialiasing FXAA** OFF
* **Antialiasing Gamma Correction** ON
* **Antialiasing Mode** OFF
* **CUDA – GPUs** ALL
* **DSR Factors** Off
* **Low Latency Mode** OFF
* **Max Frame Rate** OFF to uncap FPS
* **Monitor Technology** is an optional option. If your monitor supports G-Sync then choose it otherwise OFF
* **Multi-Frame Sampled AA (MFAA)** OFF
* **Put your main GPU in the OpenGL Rendering GPU option.**
* **Power Management Mode** Prefer Maximum Performance
* **Prefered Refresh Rate** (Your Monitor) – Select the highest available value
* **Shader Cache** On
* **Texture Filtering Anisotropic** Sample Optimization
* **Texture Filtering Negative LOD Bias** Allow
* **Texture Filtering Quality** High Performance
* **Texture Filtering** Trilinear Optimization
* **Threaded Optimization** On
* **Triple Buffering**
* **Vertical Sync (VSYNC)** OFF
* **Virtual Reality pre-rendered Frames** 1


## Toggle game features (winreg)

- [toggle windows gaming features](/reg_win_gamefeatures)

- toggle windows game_mode on/off
- toggle windows game_bar on/off
- toggle game recording

* Can potentially increase performance

## git sparse checkout

`git clone --sparse --no-checkout https://bitbucket.org/jakobhalling/gaming.git .`

`git sparse-checkout set csgo`

Will checkout only csgo folder
